import time
import tensorflow as tf

import core.utils as utils
from core.yolov4 import filter_boxes
#from PIL import Image
import cv2
import sys
from threading import Thread, Lock
import numpy as np
import argparse
import os
import simpleaudio as sa
compte = 0
non_sigal = 0 
compte_exit = 0 
def _interval_overlap(interval_a, interval_b):
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2,x4) - x1
    else:
        if x2 < x3:
             return 0
        else:
            return min(x2,x4) - x3    

def bbox_iou(box1, box2):
    intersect_w = _interval_overlap([box1[1], box1[3]], [box2[1], box2[3]])
    intersect_h = _interval_overlap([box1[0], box1[2]], [box2[0], box2[2]])  
    
    intersect = intersect_w * intersect_h

    w1, h1 = box1[3]-box1[1], box1[2]-box1[0]
    w2, h2 = box2[3]-box2[1], box2[2]-box2[0]
    
    union = w1*h1 + w2*h2 - intersect
    
    return float(intersect) / union

def _declenche_alarm(arg1,arg2, test=False):
    """
       La fonction qui declenche le son si la condition est vraie 
    """
    if test: 
        print("TEST", file=sys.stderr)
        return
    bbox_iou1 = bbox_iou(arg1,arg2)
    if bbox_iou1 > 0.0: 
        print("ALARM", file=sys.stderr)
        filename = 'test.wav'
        wave_obj = sa.WaveObject.from_wave_file(filename)
        play_obj = wave_obj.play()
        play_obj.wait_done()  # Wait until sound has finished playing

class WebcamVideoStream() :
   
    """
      la classe qui gére la lecture asynchrone des frames pour empecher le probleme 
       d'interuption premature du programme  
    """
    def __init__(self,src,width = 560, height = 560):
        self.src = src 
    
        
       
        self.stream = cv2.VideoCapture(self.src)
        (self.grabbed, self.frame) = self.stream.read()
        
          
        self.started = False
        self.read_lock = Lock()

    def start(self) :
        if self.started :
            #print "already started!!"
            return None
        self.started = True
        self.thread = Thread(target=self.update, args=())
        self.thread.start()
        return self

    def update(self) :
        global non_sigal
        global compte_exit
        while self.started :
            
            #if compte_exit>= 2 : 
                
            (grabbed, frame) = self.stream.read()
            if (grabbed == False ): 
                while grabbed != True : 

                    non_sigal = 1 
                    compte_exit += 1 
                    if compte_exit >= 5 : 
                       os._exit(0) 
                    self.stream = cv2.VideoCapture(self.src)
                    (grabbed, frame) = self.stream.read()
            if grabbed == True :
               if compte_exit >= 5 : 
                  os._exit(0)  
               non_sigal = 0
            self.read_lock.acquire()
            self.grabbed, self.frame = grabbed, frame
            #cv2.resize(self.frame, (640,480))
            self.read_lock.release()

    def read(self) :
        self.read_lock.acquire()
        frame = self.frame.copy()

        self.read_lock.release()
        return frame

    def stop(self) :
        self.started = False
        self.thread.join()
        sys.exit()

    def __exit__(self, exc_type, exc_value, traceback) :
        self.stream.release()

class yolo_cam(Thread):
    def __init__(self, src=0, detect=0,  weights = '0' , gui = 1 ):

      self.src = src 
      self.detect = detect
      self.weights = weights 
      self.gui = gui 
      Thread.__init__(self) 
    def run(self):

        STRIDES = [16, 32] 
        ANCHORS = [23,27, 37,58, 81,82, 81,82, 135,169, 344,319]
        NUM_CLASS = 80
        XYSCALE = [1.05, 1.05]
        input_size = 416
        #self.video_read = WebcamVideoStream(self.src).start()
        self.video_read = cv2.VideoCapture('teste.mp4')
        interpreter = tf.lite.Interpreter(model_path=args.weights)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        active = "alarme activée pour toutes les personnes et desactivée pour les animaux et autres"
        inactive = "alarme deactivée"
        msg = ""
        count = 0
        wait = 0 
        global compte_exit  
        while True:
            start = time.time()
            count += 1
            if count > 100:
                count = 0
            # premiere mode : l'alarme est desactivé 
            global compte
            if compte == 1 :
                if msg != active:
                    msg = active
                   
                    print(msg)
                    print("DISABLED", file=sys.stderr)
                if non_sigal != 0 :
                        print("NO-SIGNAL", file=sys.stderr)
                        frame = cv2.imread('camera.jpg')
                        compte_exit += 1  
                        if compte_exit >= 5 :
                           #si l'interuption de connexion à durée plus de 5 frame(chaque frame prend environ 90ms sur raspberry ),on coupe le programme 
                           os._exit(0)
                else :
                        _,frame = self.video_read.read() 
                        #frame = cv2.resize(frame, (1200,720))   
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                #image = frame 
                frame_size = frame.shape[:2]
                image_data = cv2.resize(frame, (input_size, input_size))
                image_data = image_data / 255.
                image_data = image_data[np.newaxis, ...].astype(np.float32)
                prev_time = time.time()

                interpreter.set_tensor(input_details[0]['index'], image_data)
                interpreter.invoke()
                pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
                boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.35,
                                                input_shape=tf.constant([input_size, input_size]))
                boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
                    boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
                    scores=tf.reshape(
                        pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
                    max_output_size_per_class=50,
                    max_total_size=50,
                    iou_threshold=0.45,
                    score_threshold=0.35
                )
                pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]
                image_h, image_w, _ = frame.shape
                out_boxes, out_scores, out_classes, num_boxes = pred_bbox
                if self.gui : 
                   image = utils.draw_bbox(frame, pred_bbox)
        
                end = time.time()
                info  = end - start  
                print ( "\t \t temps en milliseconde:  {:.2f} ms ".format ((end - start)*100), "\t\t" , "fps: {:.2f}".format( 1 / ( end - start)) , "\t \t resolution: " , (1920,1080))
                person = False
                pool = False
                force = count == 100
                for i in range(num_boxes[0]):
                    if int(out_classes[0][i]) < 0 or int(out_classes[0][i]) > NUM_CLASS: continue
                    coor = out_boxes[0][i]
                    
                    coor[0] = int(coor[0] * image_h)
                    coor[2] = int(coor[2] * image_h)
                    coor[1] = int(coor[1] * image_w)
                    coor[3] = int(coor[3] * image_w)
                    score = out_scores[0][i]
                    class_ind = int(out_classes[0][i])
                    if class_ind == 1 : 
                        pool = [coor[1], coor[0], coor[3], coor[2]]
                        if self.detect and self.detect == 1:
                            print("POOL", file=sys.stderr)
                            compte = 2
                    else:   
                        if score >= 0.5 :  
                           person = [coor[1], coor[0], coor[3], coor[2]]
                           if self.detect and self.detect == 2:
                              print("PERSON", file=sys.stderr)
                if not self.detect:
                    if (person and pool) or force:
                        _declenche_alarm(person, pool, test=force)
                    else:
                        if person:
                            print("PERSON", file=sys.stderr)
                        elif pool:
                            print("POOL", file=sys.stderr)       
                #c1, c2 = (coor[1], coor[0]), (coor[3], coor[2])
                
            elif compte == 0 : 
                if msg != inactive:
                    msg = inactive
                   
                    print(msg)
                    print("DISABLED", file=sys.stderr)
                if non_sigal != 0 :
                    print("NO-SIGNAL", file=sys.stderr)
                    image = cv2.imread('camera.jpg')
                    compte_exit += 1  
                    if compte_exit >= 5 :  #si l'interuption de connexion à durée plus de 5 frame(chaque frame prend environ 90ms sur raspberry ),on coupe le programme 
                       os._exit(0)
                else :
                    #image = self.video_reader.read()
                    _ , frame = self.video_read.read()

                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                image = frame + 0 
                frame_size = frame.shape[:2]
                image_data = cv2.resize(frame, (input_size, input_size))
                image_data = image_data / 255.
                image_data = image_data[np.newaxis, ...].astype(np.float32)
                prev_time = time.time()

                interpreter.set_tensor(input_details[0]['index'], image_data)
                interpreter.invoke()
                pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
                boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.35,
                                                input_shape=tf.constant([input_size, input_size]))
                boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
                    boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
                    scores=tf.reshape(
                        pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
                    max_output_size_per_class=50,
                    max_total_size=50,
                    iou_threshold=0.45,
                    score_threshold=0.35
                )
                pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]
                image_h, image_w, _ = frame.shape
                out_boxes, out_scores, out_classes, num_boxes = pred_bbox
                person = False
                pool = False
                for i in range(num_boxes[0]):
                    if int(out_classes[0][i]) < 0 or int(out_classes[0][i]) > NUM_CLASS: continue
                    coor = out_boxes[0][i]
                    coor[0] = int(coor[0] * image_h)
                    coor[2] = int(coor[2] * image_h)
                    coor[1] = int(coor[1] * image_w)
                    coor[3] = int(coor[3] * image_w)
                    score = out_scores[0][i]
                    class_ind = int(out_classes[0][i])
                    if class_ind == 1 : 
                        pool = [coor[1], coor[0], coor[3], coor[2]]
                        if self.detect and self.detect == 1:
                            print("POOL", file=sys.stderr)
                            compte = 2
                    else:   
                        if score >= 0.5 :  
                           person = [coor[1], coor[0], coor[3], coor[2]]
                           if self.detect and self.detect == 2:
                              print("PERSON", file=sys.stderr)
                if not self.detect:
                    if (person and pool) :
                       bbox_iou1 = bbox_iou(person,pool)
                       if bbox_iou1 > 0.0: 
                          wait = 0
                       else : 
                            wait = wait + 1 
                            time.sleep(1)

                    elif pool and not person:
                        
                        wait = wait + 1 
                        time.sleep(1) # on compte une seconde 
                if wait >= 470 : # on attend 8 minutes pour relancer l'alarme 
                   """
                    On réactive l'alarme et remettre le compteur à 0 
                   """
                   compte = 1  
                   wait = 0
                
            if self.gui : 
                result = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                #cv2.namedWindow("result", cv2.WINDOW_AUTOSIZE)
                result = cv2.resize(result, (1000,750))
                cv2.imshow("--- ALARME BIOPOOLTECH ---  VERSION 1.0", result)
                if cv2.waitKey(1) & 0xFF == ord('q'): break
                
                
def main(args) :
        thread1 = yolo_cam( detect=int(args.detect), src = args.url , weights = args.weights , gui = args.gui)
        thread1.start()
        print("READY")
        while True:
            command = input(">")
            try:
                global compte
                compte = int(command)
            except (TypeError, ValueError):
                print("bad command: %s" % command)
            if compte not in (0, 1):
                break
            
        thread1.join()
        
if __name__ == '__main__':
 argparser = argparse.ArgumentParser(description='Predict with a trained yolo model')
 argparser.add_argument('-c', '--conf', help='path to configuration file',default='config.json')
 argparser.add_argument('-u', '--url', help='Camera rtsp url',default= 'teste.mp4')#prtsp://192.168.1.14/1')
 argparser.add_argument('-d', '--detect', help='Detection type:\n\t0: alarm\n\t1: pool\n\t\t2: person',default="0")
 argparser.add_argument('-w', '--weights', help = 'path to load file ', default = './checkpoints/yolov4-416.tflite')
 argparser.add_argument('-g','--gui',help = 'display graphique interface ', default = 1 )
 args = argparser.parse_args()
 #declenche_GPU()
 main(args)
